import { FormBox } from "./FormBox";
import { render,screen } from "@testing-library/react";

const props = {
    getAllData: jest.fn(),
    button: "Default",
    setButton: jest.fn(),
    ENDPOINT: "api/",
    row: undefined,
    setFirstname: jest.fn(),
    setLastname: jest.fn(),
    firstname: undefined,
    lastname: undefined,
  };

describe.skip("component should not render at first ",()=>{
    test.skip("no render at default",()=>{
         render(<FormBox {...props}/>)

        const formElement=screen.queryByRole("form")
        expect(formElement).not.toBeInTheDocument()
    })
})

////////////////////////////////////////////////////////////////////////////////////////////////////


// import { FormBox } from "./FormBox";
// import { render, screen } from "@testing-library/react";
// import { useState } from "react";

// const FormBoxProp =()=>{
//   const [firstname, setFirstname] = useState("adam");
//   const [lastname, setLastname] = useState("jack");
//   const [button, setButton] = useState("");

//   return <FormBox setButton={setButton} setFirstname={setFirstname} setLastname={setLastname} firstname={firstname} lastname={lastname} />
// }


// const props = {
//  getAllData: jest.fn(),
//   setButton: jest.fn(),
//    ENDPOINT: "api/",
//   // setFirstname: jest.fn(),
//   // setLastname: jest.fn(),
//   ///////////
//   button: "Default",
//   row: undefined,
//   // firstname: undefined,
//   // lastname: undefined
// };
// const getAllData = jest.fn()
// const setButton= jest.fn()
// const setFirstname= jest.fn()
// const setLastname= jest.fn()

// describe.skip("component should not render at first ",()=>{
//   test.skip("no render at default",()=>{
//     render(<FormBox setLastname={setLastname} setButton={setButton} setFirstname={setFirstname} getAllData={getAllData}{...props} />)

//     const formElement = screen.queryByRole("form");
//     expect(formElement).not.toBeInTheDocument();
//   });
// });







////////////////////////////////////////////////////////////////////////////////////////////////////

// import React from 'react';
// import { render, fireEvent, screen } from '@testing-library/react';
// import { FormBox } from './FormBox';

// describe('FormBox component', () => {
//   test('renders correctly for Default', () => {
//     const getAllData = jest.fn();
//     const setButton = jest.fn();
//     const setFirstname = jest.fn();
//     const setLastname = jest.fn();
//     const ENDPOINT = 'api/';
  
//     render(
//       <FormBox
//         getAllData={getAllData}
//         button="Default"
//         setButton={setButton}
//         ENDPOINT={ENDPOINT}
//         row={undefined}
//         setFirstname={setFirstname}
//         setLastname={setLastname}
//         firstname={undefined}
//         lastname={undefined}
//       />
//     );
  
//     expect(screen.queryByText("Please select an item to delete")).not.toBeInTheDocument();
//     expect(screen.queryByText("Please fill the form to create")).not.toBeInTheDocument();
//     expect(screen.queryByText("Please select an item to edit then fill the form")).not.toBeInTheDocument();
//     expect(screen.queryByText("First Name")).not.toBeInTheDocument();
//     expect(screen.queryByText("Last Name")).not.toBeInTheDocument();
//   });

//   test('renders correctly for Delete', () => {
//     const getAllData = jest.fn();
//     const setButton = jest.fn();
//     const setFirstname = jest.fn();
//     const setLastname = jest.fn();
//     const ENDPOINT = 'api/';
  
//     render(
//       <FormBox
//         getAllData={getAllData}
//         button="Delete"
//         setButton={setButton}
//         ENDPOINT={ENDPOINT}
//         row={'1'}
//         setFirstname={setFirstname}
//         setLastname={setLastname}
//         firstname={undefined}
//         lastname={undefined}
//       />
//     );
  
//     expect(screen.getByText("Please select an item to delete")).toBeInTheDocument();
//     expect(screen.queryByText("Please fill the form to create")).not.toBeInTheDocument();
//     expect(screen.queryByText("Please select an item to edit then fill the form")).not.toBeInTheDocument();
//     expect(screen.queryByText("First Name")).not.toBeInTheDocument();
//     expect(screen.queryByText("Last Name")).not.toBeInTheDocument();
//     expect(screen.getByText("Delete")).toBeInTheDocument();
//     expect(screen.getByText("Back")).toBeInTheDocument();
//   });

// });
